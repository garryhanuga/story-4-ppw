from django.urls import include, path
from .views import formku
from .views import readmatkul
from .views import hapusjadwal,detailjadwal


urlpatterns = [
    path('', formku, name='formku'),
    path('read/', readmatkul, name='readmatkul'),
    path('read/<int:id>', hapusjadwal, name='hapusjadwal'),
    path('detail/<int:id>', detailjadwal, name='detailjadwal')
]

