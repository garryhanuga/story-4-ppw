from django.db import models

# Create your models here.
class Matkul(models.Model):
    nama_matkul= models.CharField(max_length=25)
    dosen_pengajar = models.CharField(max_length=25)
    jumlah_sks = models.CharField(max_length=1)
    deskripsi = models.TextField(max_length=100)
    semester_tahun=models.CharField(max_length=25)
    
