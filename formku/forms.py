from django.forms import ModelForm
from  .models import Matkul

class Matkulform(ModelForm):
	class Meta:
		model = Matkul
		fields = '__all__'
	error_messages = {
		'required' : 'Please Type'
	}
	