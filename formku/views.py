from django.shortcuts import render
from .forms import Matkulform
from .models import Matkul
from django.http import HttpResponse


# Create your views here.
def formku(request):
	if request.method=='POST':
		form = Matkulform(request.POST)
		if form.is_valid():
			data = Matkul()
			data.nama_matkul = form.cleaned_data['nama_matkul']
			data.dosen_pengajar = form.cleaned_data['dosen_pengajar']
			data.deskripsi = form.cleaned_data['deskripsi']
			data.jumlah_sks = form.cleaned_data['jumlah_sks']
			data.semester_tahun = form.cleaned_data['semester_tahun']
			data.save()
			
		return render(request, 'form.html', {'form' : form})
	else:
		form=Matkulform()
		return render(request, 'form.html', {'form' : form})

    # form= Matkulform(request.POST)
    # if form.is_valid():
        # form.save()
    # context={'form': form}
    # return render(request,'form.html',context)

def hapusjadwal(request,id ):
    Matkul.objects.filter(id=id).delete()
    matkuls = Matkul.objects.all()
    return render(request, 'read.html', {'matkuls':matkuls})


def readmatkul(request):
	matkuls = Matkul.objects.all()
	# response['matkuls'] = matkuls

	html = 'read.html'
	return render(request, html, {'matkuls':matkuls})

def detailjadwal(request,id):
	matkul=Matkul.objects.get(id=id)
	html = 'detail.html'
	return render(request, html, {'matkul':matkul})




