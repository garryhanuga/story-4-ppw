from django.apps import AppConfig


class FormkuConfig(AppConfig):
    name = 'formku'
